// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "fractional.h"

#include <time.h>

#ifndef OUTPUT_FREQUENCY
#define OUTPUT_FREQUENCY 1
#endif

/****************************************************************************
 *                      Initial Conditions
 ****************************************************************************/
static variable_t mittag_leffler_f(double t, variable_t y, double alpha) {
    UNUSED(t);
    UNUSED(alpha);
    return -y;
}

static variable_t mittag_leffler_y0(double t) {
    UNUSED(t);
    return 1.0;
}

static variable_t example1_f(double t, variable_t y, double alpha) {
    return pow(t, 4.0) + 1.0 + 24.0 * pow(t, 4 - alpha) / tgamma(5 - alpha) - y;
}

static variable_t example1_y0(double t) {
    UNUSED(t);
    return pow(t, 4.0) + 1;
}

/****************************************************************************
 *                      Public Functions
 ****************************************************************************/
fractional_cache_t *fractional_cache_new(fractional_data_t *data) {
    const double alpha = data->alpha;
    const double gamma0 = tgamma(alpha + 1);
    const double gamma1 = tgamma(alpha + 2);
    const double alpha1 = alpha + 1;

    size_t n = data->num_global / data->mpisize;
    fractional_cache_t *cache = utils_malloc(sizeof(fractional_cache_t));

    /* allocate */
    /* TODO: these should only allocate data->num_local or something */
    cache->a = utils_malloc(data->num_global * sizeof(double));
    cache->b = utils_malloc(data->num_global * sizeof(double));
    cache->c = utils_malloc(data->num_global * sizeof(double));
    cache->fy = utils_malloc(n * sizeof(variable_t));

    /* Initialize the coefficients necessary for the current process. Each process
     * p needs all the coefficients for n belonging to p' >= p
     */
    for (size_t j = 0; j < data->num_global; ++j) {
        cache->a[j] =
            (pow(j + 2, alpha1) - 2 * pow(j + 1, alpha1) + pow(j, alpha1)) / gamma1;
        cache->b[j] = (pow(j + 1, alpha) - pow(j, alpha)) / gamma0;
        cache->c[j] = (pow(j, alpha1) - (j - alpha) * pow(j + 1, alpha)) / gamma1;
    }

    /* initialize fy */
    if (data->mpirank == 0) {
        cache->fy[0] = data->f0;
    }

    return cache;
}

void fractional_cache_destroy(fractional_cache_t *cache) {
    utils_free(cache->a);
    utils_free(cache->b);
    utils_free(cache->c);
    utils_free(cache->fy);

    utils_free(cache);
}

fractional_data_t *fractional_data_new(
    const char *id, size_t n, double t0, double alpha, MPI_Comm mpicomm
) {
    fractional_data_t *data = utils_malloc(sizeof(fractional_data_t));

    /* save the MPI info */
    data->mpicomm = mpicomm;
    MPI_Comm_size(mpicomm, &(data->mpisize));
    MPI_Comm_rank(mpicomm, &(data->mpirank));

    /* allocate the arrays */
    data->num_global = n * data->mpisize;
    data->y = utils_malloc(n * sizeof(variable_t));

    /* initialize */
    if (strcmp(id, "mittag_leffler") == 0) {
        data->f = mittag_leffler_f;
        data->y0 = mittag_leffler_y0(t0);
    } else if (strcmp(id, "example1") == 0) {
        data->f = example1_f;
        data->y0 = example1_y0(t0);
    } else {
        ABORT("Invalid id for the initial condition: \"%s\".", id);
    }

    data->alpha = alpha;
    data->t0 = t0;
    data->f0 = data->f(data->t0, data->y0, alpha);
    data->y[0] = data->y0;

    return data;
}

void fractional_data_destroy(fractional_data_t *data) {
    utils_free(data->y);
    utils_free(data);
}

variable_t fractional_data_solution(fractional_data_t *data, size_t i) {
    return data->y[i];
}

static double fractional_convolution(
    double *b, double *f, size_t N, size_t j, size_t k, int mpisize, int n
) {
    double sum = 0;

    while (j < N) {
        sum += b[n - k - 1] * f[j];

        ++j;
        k += mpisize;
    }

    return sum;
}

void fractional_step(
    fractional_data_t *data, size_t n, double dt, fractional_cache_t *cache
) {
    const double alpha = data->alpha;
    const double h = pow(dt, alpha);
    const int mpisize = data->mpisize;
    const int mpirank = data->mpirank;

    size_t end = 0;
    function_t f = data->f;
    variable_t *y = data->y;

    double *a = cache->a;
    double *b = cache->b;
    double *c = cache->c;
    double *fy = cache->fy;

    variable_t yp;
    int pp = 0;
    double sum = 0;
    double sump = 0;

    double time;

    /* initialize timer for current iteration */
    time = (double) clock();

    /* which process is responsible for the current iteration */
    pp = n % mpisize;

    /* how many elements to sum for the current process */
    end = n / mpisize + (mpirank >= pp ? 0 : 1);

    /* compute the predictor */
    sump = fractional_convolution(b, fy, end, 0, mpirank, mpisize, n);
    MPI_Reduce(&sump, &sum, 1, MPI_DOUBLE, MPI_SUM, pp, data->mpicomm);

    if (mpirank == pp) {
        yp = data->y0 + h * sum;
    }

    /* compute the corrector */
    /* NOTE: global convolution sum starts at 1 for the corrector */
    if (mpirank == 0) {
        sump = fractional_convolution(a, fy, end, 1, mpisize, mpisize, n);
    } else {
        sump = fractional_convolution(a, fy, end, 0, mpirank, mpisize, n);
    }
    sum = 0;
    MPI_Reduce(&sump, &sum, 1, MPI_DOUBLE, MPI_SUM, pp, data->mpicomm);

    /* compute time for the current iteration */
    time = ((double) clock() - time) / CLOCKS_PER_SEC;
    if (data->mpirank == 0) {
        MPI_Reduce(MPI_IN_PLACE, &time, 1, MPI_DOUBLE, MPI_SUM, 0, data->mpicomm);
    } else {
        MPI_Reduce(&time, &time, 1, MPI_DOUBLE, MPI_SUM, 0, data->mpicomm);
    }
    time = time / data->mpisize;

    if (mpirank == pp) {
        y[end] = data->y0 + h * (c[n - 1] * data->f0 + sum +
                                 f(n * dt, yp, alpha) / tgamma(alpha + 2));
        fy[end] = data->f(n * dt, y[end], alpha);

        if (n % OUTPUT_FREQUENCY == 0) {
            LOG_DEBUG(
                "Iteration %10lu / Process %4d / t %10.5g / time %g",
                n,
                pp,
                n * dt,
                time
            );
        }
    }
}
