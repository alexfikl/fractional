// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "workspace.h"

#include <getopt.h>

static struct option options[] = {
    {"filename", required_argument, 0, 'f'},
    {"list-initial", no_argument, 0, 'l'},
    {"initial", required_argument, 0, 'i'},
    {"t0", required_argument, 0, 't'},
    {"tn", required_argument, 0, 'T'},
    {"alpha", required_argument, 0, 'a'},
    {"steps", required_argument, 0, 'n'},
    {"help", no_argument, 0, 'h'},
    {0, 0, 0, 0}
};

static void usage(void) {
    if (!utils_is_root()) {
        return;
    }

    printf("Usage: fractional [OPTIONS] [-h].\n");
    printf("\nOptions:\n");
    printf("\t-f, --filename     Filename to save solution (default: fractional.dat)\n"
    );
    printf("\t-l, --list-initial List initial conditions\n");
    printf("\t-i, --initial      Name of initial condition (default: mittag_leffler)\n"
    );
    printf("\t-t, --t0           Starting time (default: 0)\n");
    printf("\t-T, --tn           Ending time (default: 1)\n");
    printf("\t-a, --alpha        Order of the derivative (default: 0.5)\n");
    printf("\t-n, --steps        Number of points per process (default: 100)\n");
    printf("\t-h, --help         Display help message\n");
    printf("\nTHIS PROGRAM COMES WITH ABSOLUTELY NO WARRANTY.\n");
}

static void list_initial_conditions(void) {
    if (!utils_is_root()) {
        return;
    }

    printf("Initial conditions:\n");
    printf("mittag_leffler\n");
    printf("example1\n");
    printf("\nFor details see the docs in fractional.h\n");
}

static void workspace_handle_options(workspace_t *w, int argc, char **argv) {
    int c = 0;
    int option_index = 0;

    /* set default values */
    w->filename = NULL;
    w->ic = NULL;
    w->t0 = 0.0;
    w->tn = 1.0;
    w->alpha = 0.5;
    w->n = 100;

    while (1) {
        c = getopt_long(argc, argv, "f:li:n:t:T:a:h", options, &option_index);
        if (c == -1) {
            break;
        }

        switch (c) {
        case 'f':
            w->filename = utils_strdup(optarg);
            break;
        case 'l':
            list_initial_conditions();
            MPI_Finalize();
            exit(0);
        case 'i':
            w->ic = utils_strdup(optarg);
            break;
        case 't':
            w->t0 = atof(optarg);
            break;
        case 'T':
            w->tn = atof(optarg);
            break;
        case 'a':
            w->alpha = atof(optarg);
            break;
        case 'n':
            w->n = atol(optarg);
            break;
        case 'h':
            usage();
            MPI_Finalize();
            exit(0);
        case '?':
            break;
        default:
            LOG_GLOBAL("Unrecognized option: %d.\n", c);
        }
    }

    /* if no filename has been given, use a default one */
    if (w->filename == NULL) {
        w->filename = utils_strdup("fractional.dat");
    }

    /* if no initial condition has been given, use a default one */
    if (w->ic == NULL) {
        w->ic = utils_strdup("mittag_leffler");
    }

    /* compute the time steps */
    w->n = (w->n <= 1 ? 2 : w->n);
    w->dt = (w->tn - w->t0) / ((w->n - 1) * w->mpisize + 1);

    LOG_DEBUG_GLOBAL("processes:  %d", w->mpisize);
    LOG_DEBUG_GLOBAL("filename:   \"%s\"", w->filename);
    LOG_DEBUG_GLOBAL("ic:         \"%s\"", w->ic);
    LOG_DEBUG_GLOBAL("time:       [%g, %g]", w->t0, w->tn);
    LOG_DEBUG_GLOBAL("points:     %lu", w->n * w->mpisize);
    LOG_DEBUG_GLOBAL("dt:         %g", w->dt);
}

workspace_t *workspace_new(MPI_Comm mpicomm, int argc, char **argv) {
    workspace_t *w = utils_malloc(sizeof(workspace_t));

    /* init MPI stuff */
    w->mpicomm = mpicomm;
    MPI_Comm_size(mpicomm, &(w->mpisize));
    MPI_Comm_rank(mpicomm, &(w->mpirank));

    /* command line options */
    workspace_handle_options(w, argc, argv);
    w->data = fractional_data_new(w->ic, w->n, w->t0, w->alpha, w->mpicomm);

    return w;
}

void workspace_destroy(workspace_t *w) {
    fractional_data_destroy(w->data);

    utils_free(w->filename);
    utils_free(w->ic);
    utils_free(w);
}

void workspace_solve(workspace_t *w) {
    fractional_cache_t *cache = fractional_cache_new(w->data);
    size_t N = w->n * w->mpisize;

    for (size_t i = 1; i < N; ++i) {
        fractional_step(w->data, i, w->dt, cache);
    }

    fractional_cache_destroy(cache);
}

void workspace_save(workspace_t *w) {
    int mpiret;

    MPI_File file;
    int offset = 2 * w->mpirank * sizeof(double);

    size_t n = w->n;
    double t = w->t0 + w->mpirank * w->dt;
    double dt = w->mpisize * w->dt;

    size_t data_size = 0;
    double *data = NULL;

    /* gather the data */
    data_size = 2 * n * sizeof(double);
    data = utils_malloc(data_size);
    for (size_t i = 0; i < n; ++i, t += dt) {
        data[2 * i + 0] = t;
        data[2 * i + 1] = fractional_data_solution(w->data, i);
    }

    /* open the file */
    mpiret = MPI_File_open(
        MPI_COMM_WORLD,
        w->filename,
        MPI_MODE_WRONLY | MPI_MODE_CREATE,
        MPI_INFO_NULL,
        &file
    );
    CHECK_MPI(mpiret);

    /* create a view. since each process has iteration n = P * j + p, we need
     * to write to the file as follows:
     *    y_{0, 0}, y_{0, 1}, ..., y_{0, P},
     *    y_{1, 0}, y_{1, 1}, ..., y_{1, P},
     *    ...
     *    y_{n, 0}, y_{n, 1}, ..., y_{n, P}
     * this special view is defined by UTILS_MPI_FILETYPE.
     */
    mpiret = MPI_File_set_view(
        file, offset, MPI_DOUBLE, UTILS_MPI_FILETYPE, "native", MPI_INFO_NULL
    );
    CHECK_MPI(mpiret);

    /* write it */
    mpiret = MPI_File_write(file, data, 2 * n, MPI_DOUBLE, MPI_STATUS_IGNORE);
    CHECK_MPI(mpiret);

    MPI_File_close(&file);
    utils_free(data);
}
