// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "utils.h"

static int utils_identifier = -1;
static int utils_malloc_count = 0;

MPI_Datatype UTILS_MPI_FILETYPE = MPI_DOUBLE;

static void utils_create_mpi_datatype(int mpisize) {
    MPI_Datatype type;
    int extent = 2 * mpisize * sizeof(double);
    int mpiret;

    mpiret = MPI_Type_contiguous(2, MPI_DOUBLE, &type);
    CHECK_MPI(mpiret);
    mpiret = MPI_Type_create_resized(type, 0, extent, &UTILS_MPI_FILETYPE);
    CHECK_MPI(mpiret);
    mpiret = MPI_Type_commit(&UTILS_MPI_FILETYPE);
    CHECK_MPI(mpiret);
}

void utils_init(MPI_Comm mpicomm, int *argc, char ***argv) {
    int mpisize = 0;
    int mpiret = 0;
    utils_malloc_count = 0;

    mpiret = MPI_Init(argc, argv);
    CHECK_MPI(mpiret);
    mpiret = MPI_Comm_rank(mpicomm, &utils_identifier);
    CHECK_MPI(mpiret);
    mpiret = MPI_Comm_size(mpicomm, &mpisize);
    CHECK_MPI(mpiret);

    utils_create_mpi_datatype(mpisize);
}

void utils_finalize(void) {
    int mpiret;

    if (utils_malloc_count != 0) {
        LOG_WARNING("Uneven malloc / free calls.");
    }

    mpiret = MPI_Finalize();
    CHECK_MPI(mpiret);
}

void *utils_malloc(size_t size) {
    void *p = malloc(size);
    CHECK_MEMORY(p);

    ++utils_malloc_count;
    return p;
}

void *utils_calloc(size_t n, size_t size) {
    void *p = calloc(n, size);
    CHECK_MEMORY(p);

    ++utils_malloc_count;
    return p;
}

char *utils_strdup(const char *str) {
    char *p = utils_malloc(strlen(str) + 1);
    strcpy(p, str);

    return p;
}

void utils_free(void *p) {
    --utils_malloc_count;
    free(p);
}

int utils_is_root(void) {
    return utils_identifier <= 0;
}
