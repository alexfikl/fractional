// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __WORKSPACE_H__
#define __WORKSPACE_H__

#include "fractional.h"

/**
 * \brief Main structure that contains the state of the simulation.
 */
typedef struct {
    MPI_Comm mpicomm;
    int mpisize;
    int mpirank;

    char *filename; /**< Output filename. */
    char *ic;       /**< Initial condition. */
    double t0, tn;  /**< Time interval. */
    double alpha;   /**< Order of the derivative. */

    size_t n;  /**< Iterations. */
    double dt; /**< Time step. */

    fractional_data_t *data;
} workspace_t;

/**
 * \brief Initialize a new workspace.
 */
workspace_t *workspace_new(MPI_Comm mpicomm, int argc, char **argv);

/**
 * \brief Free all the allocated data.
 */
void workspace_destroy(workspace_t *w);

/**
 * \brief Compute the solution at all time steps.
 */
void workspace_solve(workspace_t *w);

/**
 * \brief Save the solution to the given file.
 *
 * The solution is saved in pairs \f$(t_n, y(t_n))\f$. Can be opened in \c gnuplot or
 * others.
 *
 * The format is binary, written using MPI I/O.
 */
void workspace_save(workspace_t *w);

#endif /* __WORKSPACE_H__ */
