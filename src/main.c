// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "workspace.h"

#include <time.h>

int main(int argc, char **argv) {
    MPI_Comm mpicomm = MPI_COMM_WORLD;

    workspace_t *w = NULL;
    double time = 0;

    /* initialize */
    utils_init(mpicomm, &argc, &argv);

    /* create a new workspace */
    w = workspace_new(mpicomm, argc, argv);

    /* solve our fractional differential equation */
    time = (double) clock();
    workspace_solve(w);
    time = ((double) clock() - time) / CLOCKS_PER_SEC;

    if (w->mpirank == 0) {
        MPI_Reduce(MPI_IN_PLACE, &time, 1, MPI_DOUBLE, MPI_SUM, 0, mpicomm);
    } else {
        MPI_Reduce(&time, &time, 1, MPI_DOUBLE, MPI_SUM, 0, mpicomm);
    }

    LOG_GLOBAL("[LOG] time: average %g / total %g", time / w->mpisize, time);

    /* write everything to a file */
    workspace_save(w);

    /* destroy the workspace */
    workspace_destroy(w);

    /* finalize everything */
    utils_finalize();

    return EXIT_SUCCESS;
}
