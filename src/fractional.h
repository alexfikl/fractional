// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __FRACTIONAL_H__
#define __FRACTIONAL_H__

#include "utils.h"

/**
 * \brief Define a type for our variable.
 */
typedef double variable_t;

/**
 * \brief Prototype for the right hand side.
 */
typedef variable_t (*function_t)(double t, variable_t y, double alpha);

/**
 * \brief Struct describing the data necessary for the algorithm.
 */
typedef struct {
    MPI_Comm mpicomm;
    int mpisize;
    int mpirank;

    function_t f; /**< right hand side */
    double alpha;

    double t0;     /**< initial time */
    variable_t y0; /**< initial condition at t0 */
    variable_t f0; /**< f(t0, y0) */

    size_t num_global;
    variable_t *y; /**< local array of variables */
} fractional_data_t;

/**
 * \brief Allocate the variables and set the initial condition.
 *
 * Available initial condition ids are listed below.
 *
 * <table>
 * <tr>
 *      <td>mittag_leffler</td>
 *      <td>\f$f(t, y) = -y\f$ and \f$y(t_0) = E_{\alpha}(t_0)\f$</td>
 * </tr>
 * </table>
 *
 * \param[in] id    The id of the initial condition.
 * \param[in] n     The number of iterations / number of variables we need.
 * \param[in] t0    The star time at which to compute the initial condition.
 * \return A fully allocated struct.
 */
fractional_data_t *fractional_data_new(
    const char *id, size_t n, double t0, double alpha, MPI_Comm mpicomm
);

/**
 * \brief Free the data.
 */
void fractional_data_destroy(fractional_data_t *data);

/**
 * \brief Return the solution for the nth time step on the current process.
 */
variable_t fractional_data_solution(fractional_data_t *data, size_t n);

/**
 * \brief Cache to avoid recomputing all the coefficients and function calls.
 */
typedef struct {
    double *a;
    double *b;
    double *c;

    variable_t *fy;
} fractional_cache_t;

/**
 * \brief Allocate the struct and precompute the cached coefficients.
 *
 * The cache is given by:
 *
 * \f[
 * \begin{aligned}
 *      a_j & = \frac{
 *          (j + 2)^{\alpha + 1} -
 *          2 (j + 1)^{\alpha + 1} + j^{\alpha + 1}}{\Gamma(\alpha + 2)}, \\
 *      b_j & = \frac{(j + 1)^{\alpha} - j^{\alpha}}{\Gamma(\alpha + 1)}, \\
 *      c_j & = \frac{j^{\alpha + 1} - (j - \alpha)(j + 1)^\alpha}{\Gamma(\alpha + 2)}.
 * \end{aligned}
 * \f]
 *
 * The values of \f$f(t, y)\f$ will be cached as they are computed by
 * the numerical scheme.
 *
 * \param[in] N         The number of coarse points.
 * \param[in] n         The number of fine points.
 * \param[in] alpha     The order of the derivative.
 * \return A fully allocated and initialized struct.
 */
fractional_cache_t *fractional_cache_new(fractional_data_t *data);

/**
 * \brief Free the cache.
 */
void fractional_cache_destroy(fractional_cache_t *cache);

/**
 * \brief Advance from the step n - 1 to step n.
 *
 * We are trying to solve the fractional differential equation:
 * \f[
 *      D^\alpha y(t) = f(t, y(t)).
 * \f]
 * for an order \f$\alpha \in [0, 1]\f$ and by using a predictor-corrector scheme
 * (Adams-Bashforth-Moulton):
 *
 * \f[
 * \begin{aligned}
 * y^P_n & = y_0 + \Delta t^\alpha \sum_{j = 0}^{n - 1} b_{n - j - 1} f(t_j, y^C_j), \\
 * y^C_n & = y_0 + \Delta t^\alpha (c_{n - 1} f(t_0, y_0) +
 *                          \sum_{j = 1}^{n - 1} a_{n - j - 1} f(t_j, y^C_j) +
 *                          f(t_n, y^P_n) / \Gamma(\alpha + 2)).
 * \end{aligned}
 * \f]
 *
 * The parallel algorithm divides each iteration between the \f$P\f$ processes
 * such that process p will compute all iterations of the form:
 * \f[
 *      n = P j + p, \quad \text{for } j > 0
 * \f]
 * For example, with \f$P = 4\f$, process 1 will compute iterations, 1, 5, 9, 13
 * and so on.
 *
 * Given the iteration \f$n\f$ and the number of processes \f$P\f$, we can find out
 * which process has to compute the current iteration:
 * \f[
 *          p' = n % P
 * \f]
 * as well as the number of elements that have already been computed by
 * the current process \f$p\f$
 * \f[
 *          K = \frac{n}{P} \quad \text{ or } \quad K = \frac{n}{P} + 1
 * \f]
 * depending on whether \f$p < p'\f$ or not (i.e. if we have already advanced
 * an iteration on the previous processes).
 *
 * \param[in,out] y     The solution. This is a vector of at least size \f$n + 1\f$.
 *                      \c y[0] will be used to store the predictor values, while
 *                      \c y[1] will be used for the corrector values. After the
 *                      function call, the values at position \f$n\f$ will be updated.
 * \param[in] n         The current iteration.
 * \param[in] dt        The time step.
 * \param[in] alpha     The order of the derivative, in \f$[0, 1]\f$.
 * \param[in] cache     Precomputed list of coefficients.
 *
 * \sa fractional_cache_new
 */
void fractional_step(
    fractional_data_t *data, size_t n, double dt, fractional_cache_t *cache
);

#endif /* __FRACTIONAL_H__ */
