// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <stdlib.h>

#include <complex.h>
#include <math.h>
#include <time.h>

#include <fftw3-mpi.h>

double convolution_fftw(MPI_Comm mpicomm, ptrdiff_t n, double alpha) {
    const double gamma0 = tgamma(alpha + 1);
    int mpisize;
    int mpirank;

    fftw_plan forward;
    fftw_plan backward;

    ptrdiff_t local_alloc;
    ptrdiff_t ni, si; /* input size and start */
    ptrdiff_t no, so; /* output size and start */

    fftw_complex *f;
    fftw_complex *g;
    fftw_complex *h;
    fftw_complex *fhat;
    fftw_complex *ghat;
    fftw_complex *hhat;

    MPI_Comm_size(mpicomm, &mpisize);
    MPI_Comm_rank(mpicomm, &mpirank);

    /* get size for local arrays */
    local_alloc = fftw_mpi_local_size_1d(
        n, mpicomm, FFTW_FORWARD, FFTW_ESTIMATE, &ni, &si, &no, &so
    );
    printf("[%d] local_alloc %td\n", mpirank, local_alloc);
    printf("[%d] ni %td si %td\n", mpirank, ni, si);
    printf("[%d] no %td so %td\n", mpirank, no, so);

    /* allocate arrays */
    f = fftw_alloc_complex(local_alloc);
    g = fftw_alloc_complex(local_alloc);
    h = fftw_alloc_complex(local_alloc);
    fhat = fftw_alloc_complex(local_alloc);
    ghat = fftw_alloc_complex(local_alloc);
    hhat = fftw_alloc_complex(local_alloc);

    /* create plans */
    forward = fftw_mpi_plan_dft_1d(
        n, f, fhat, mpicomm, FFTW_FORWARD, FFTW_ESTIMATE | FFTW_MPI_SCRAMBLED_OUT
    );
    backward = fftw_mpi_plan_dft_1d(
        n, hhat, h, mpicomm, FFTW_BACKWARD, FFTW_ESTIMATE | FFTW_MPI_SCRAMBLED_IN
    );

    /* fill in arrays */
    for (ptrdiff_t i = si; i < si + ni; ++i) {
        f[i - si] = (pow(i + 1, alpha) - pow(i, alpha)) / gamma0 + 0 * I;
        g[i - si] = exp(-(i * 0.00001)) + 0 * I;
    }

    /* perform the transformation */
    fftw_mpi_execute_dft(forward, f, fhat);
    fftw_mpi_execute_dft(forward, g, ghat);

    /* element by element product */
    for (ptrdiff_t i = 0; i < ni; ++i) {
        hhat[i] = fhat[i] * ghat[i] / n;
    }

    /* inverse transformation for the result => convolution */
    fftw_mpi_execute_dft(backward, hhat, h);

    char filename[BUFSIZ];
    FILE *fd;

    snprintf(filename, BUFSIZ, "fftw_mpi_%05d.txt", mpirank);
    fd = fopen(filename, "w");
    for (ptrdiff_t i = 0; i < ni; ++i) {
        fprintf(fd, "%lu %g\n", i + si, creal(h[i]));
    }
    fclose(fd);

    return 0;
}

int main(int argc, char **argv) {
    MPI_Comm mpicomm = MPI_COMM_WORLD;
    const ptrdiff_t n = 256;
    const double alpha = 0.5;

    double time;

    MPI_Init(&argc, &argv);
    fftw_mpi_init();

    time = (double) clock();
    convolution_fftw(mpicomm, n, alpha);
    printf("convolution_fftw time %gs\n", ((double) clock() - time) / CLOCKS_PER_SEC);

    fftw_mpi_cleanup();
    MPI_Finalize();

    return EXIT_SUCCESS;
}
