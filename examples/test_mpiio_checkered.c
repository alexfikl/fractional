// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const int n = 10;
static const char *filename = "mpiio.dat";

int create_filetype(int mpisize, MPI_Datatype *filetype) {
    MPI_Datatype type;
    int extent = 2 * mpisize * sizeof(double);

    MPI_Type_contiguous(2, MPI_DOUBLE, &type);
    MPI_Type_create_resized(type, 0, extent, filetype);
    return MPI_Type_commit(filetype);
}

int write_mpiio_file(int argc, char **argv) {
    MPI_Comm mpicomm = MPI_COMM_WORLD;
    int mpisize;
    int mpirank;

    MPI_File file;
    MPI_Datatype filetype;
    MPI_Status status;

    double *data;

    /* init mpi */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(mpicomm, &mpisize);
    MPI_Comm_rank(mpicomm, &mpirank);

    /* create data */
    data = malloc(2 * n * sizeof(double));
    for (int i = 0; i < n; ++i) {
        data[2 * i + 0] = mpisize * i + mpirank;
        data[2 * i + 1] = mpirank;
    }

    /* open file */
    MPI_File_open(
        mpicomm, filename, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &file
    );

    /* define the view */
    create_filetype(mpisize, &filetype);
    MPI_File_set_view(
        file,
        2 * mpirank * sizeof(double),
        MPI_DOUBLE,
        filetype,
        "native",
        MPI_INFO_NULL
    );

    /* write the file */
    MPI_File_write(file, data, 2 * n, MPI_DOUBLE, &status);
    MPI_File_close(&file);

    free(data);

    return MPI_Finalize();
}

int read_mpiio_file(int mpisize) {
    FILE *fd;
    double x, y;

    fd = fopen(filename, "rb");

    for (int i = 0; i < n * mpisize; ++i) {
        fscanf(fd, "%lf%lf", &x, &y);
        printf("[%d] %g %g\n", i, x, y);
    }

    fclose(fd);

    return EXIT_SUCCESS;
}

int main(int argc, char **argv) {
    if (strcmp(argv[1], "write") == 0) {
        return write_mpiio_file(argc, argv);
    } else if (strcmp(argv[1], "read") == 0) {
        return read_mpiio_file(atoi(argv[2]));
    }

    return EXIT_SUCCESS;
}
