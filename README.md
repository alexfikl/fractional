# Compilation

This project uses [meson](https://mesonbuild.com/) for its build system. It
can be directly combiled with
```bash
meson setup -Dbuildtype=debug -Db_sanitize=undefined build
meson compile -C build
```

# License

MIT
